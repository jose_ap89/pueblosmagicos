#!/bin/bash
echo "....RESETING DB...."
heroku pg:reset
echo "....MIGRATING...."
heroku run python manage.py migrate
echo "....LOADING DATA...."
heroku run "python manage.py loaddata pueblomagico/fixtures/*.json | sort"


