from django.contrib import admin

# Register your models here.
from .models import *

class EstadoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')

class HotelAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','direccion','pueblo_id')
    list_filter = ['pueblo_id']

class PuebloAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','estado_id')
    list_filter = ['estado_id']
    search_fields = ['nombre']

class ViajeAdmin(admin.ModelAdmin):
    list_display = ('id','fecha','pueblo_id','hotel_id','camion_id','costo')
    list_filter = ['pueblo_id']
    search_fields = ['pueblo_id']

class CamionAdmin(admin.ModelAdmin):
    list_display = ('id', 'modelo','placas','asientos')

admin.site.register(Boleto)
admin.site.register(Camion,CamionAdmin)
admin.site.register(Hotel,HotelAdmin)
admin.site.register(Viaje,ViajeAdmin)
admin.site.register(Estado,EstadoAdmin)
admin.site.register(Pueblo,PuebloAdmin)
