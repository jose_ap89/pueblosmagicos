from django.apps import AppConfig


class PueblomagicoConfig(AppConfig):
    name = 'pueblomagico'
