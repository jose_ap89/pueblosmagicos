from django.shortcuts import render, redirect
from django.contrib import messages
from ..forms import *
from ..models import Pueblo, Estado, Comentario
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils import timezone as tz

def getCommentsbyID(pueblo: "i32") -> "Generator":
    """ Returns a Generator if there is any comment, None otherwise """
    if Comentario.objects.filter(pueblo=pueblo).count() > 0 : 
        return Comentario.objects.filter(pueblo=pueblo).order_by("-modificacion").select_related()
    else:
        None

def post(request):
    """ Page that shows the filter to choose the town so that to write or read comments """
    towns = Pueblo.objects.select_related().order_by('nombre')
    states = Estado.objects.all().order_by("nombre")
    context = {
        'title': 'Informate',
        'page': "post",
        'towns': towns,
        'states': states 
    }
    return render(request, 'pueblomagico/post.html', context)

def town(request,id:'i32'):
    """ It shows the comments written for a particular town or post
        for example, post/1 refers to all comments done to the town
        with ID 1 """
    context = {
        'title': 'Informate',
        'page': "post",
        'town': Pueblo.objects.all().select_related().get(pk=id) ,
        "comments" : getCommentsbyID(id)
    }

    if request.method == 'POST':
        form = CreateCommentForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data["user"]
            texto = form.cleaned_data["texto"].strip()
            pueblo_id = form.cleaned_data["pueblo"]
            page = form.cleaned_data["currPage"]
            comment = Comentario(
                user = User.objects.get(pk=user_id),
                texto = texto,
                publicacion = tz.now(),
                modificacion = tz.now(),
                pueblo = Pueblo.objects.get(pk=pueblo_id)
            )
            comment.save()
            #message that will display a success after the post request to create a comment
            messages.add_message(request, messages.SUCCESS,
                'Comentario creado correctamente',
                extra_tags='commentCreated'
            )
            return redirect(page)
        else:
            return redirect(page)
    else:
        return render(request, 'pueblomagico/town.html' , context)


def signup(request):
    """ create user """
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data["user"]
            name = form.cleaned_data["name"].lower()
            lastname = form.cleaned_data["lastname"].lower()
            email = form.cleaned_data["email"]
            password1 = form.cleaned_data["password1"]
            page = form.cleaned_data["currPage"]
            try:
                #If there is a user with that name an error must be displayed
                User.objects.get(username=user)
                messages.add_message(request, messages.ERROR,
                        f'Error de creación de usuario. {user} es un nombre que ya existe.  Intenta crear un nuevo nombre de usuario',
                    extra_tags='signupError'
                )
                return redirect(page)
            except User.DoesNotExist:
                u = User.objects.create_user(user,password = password1)
                u.first_name = name
                u.last_name = lastname
                u.email = email
                u.save()
                #message that will display a success after the post request to create a user
                messages.add_message(request, messages.SUCCESS,
                    'Usuario creado correctamente. Ya puedes iniciar sesión para comentar',
                     extra_tags='userCreated'
                )
                return redirect(page)
    return redirect("/")
    
def login(request):
    """login user """
    if request.method == 'POST':
        form = LoginUserForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data["inputUser"]
            password = form.cleaned_data["inputPassword"]
            page = form.cleaned_data["currPage"]
            user_auth = authenticate(username=user, password=password)
            if user_auth is not None:
                user = {
                    "userid" : user_auth.id,
                    "username": user_auth.username,
                    "firstname":user_auth.first_name,
                    "lastname" : user_auth.last_name,
                    "email" : user_auth.email
                }
                # user session has being created
                request.session["user"] = user
                messages.add_message(request, messages.SUCCESS,
                    f'Cuenta del usuario {user["username"]} iniciada correctamente',
                    extra_tags='loginSuccess'
                )
                return redirect(page)
            else:
                messages.add_message(request, messages.ERROR,
                    f'Error de acceso a cuenta de usuario. El usuario {user} no existe o se proporciono datos de acceso erróneos. Proporcione correctamente sus datos o cree una cuenta',
                    extra_tags='loginError'
                )
                return redirect(page)
    return redirect("/")

def logout(request):
    "logout user"
    if request.method == 'POST':
        if request.POST.get("logout") == "Ok":
            messages.add_message(request, messages.INFO,
                f'La sesión del  usuario {request.session["user"]["username"]} ha finalizado',
                extra_tags='logoutSuccess'
            )
            del request.session['user']
            return redirect(request.POST.get("currPage"))
    else:
        return redirect("/")

def editComment(request,id : "i32",comment : "i32"):
    """ id is the town's id and comment is the comment's id """
    if request.method == "POST":
        form = EditCommentForm(request.POST)
        if form.is_valid():
            page = form.cleaned_data["currPage"]
            method = form.cleaned_data["_method"]
            currComment = Comentario.objects.get(pk=comment)
            print(comment,page,method, currComment)
            if method == "GET":
                return render(request, 'pueblomagico/editComment.html', {
                    "comment" : currComment,
                    "town" : Pueblo.objects.get(pk=id)
                })
            elif method == "PUT":
                texto = form.cleaned_data["texto"].strip()
                Comentario.objects.filter(id=comment).update(texto=texto, modificacion=tz.now())
                #message that will display a success after the post request to edit a comment
                messages.add_message(request, messages.SUCCESS,
                    'Comentario editado correctamente',
                    extra_tags='commentEdited'
                )
                return redirect(f"/post/{id}")
            elif method == "DELETE": 
                currComment.delete()
                #message that will display a success after the post request to delete a comment
                messages.add_message(request, messages.SUCCESS,
                    'Comentario borrado correctamente',
                    extra_tags='commentDeleted'
                )
                return redirect(f"/post/{id}")
            else:
                return redirect(page)
        else:
            return redirect(f"/post/{id}")    
    else:
        return redirect(f"/post/{id}")
