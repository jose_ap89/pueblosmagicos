from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.utils import timezone as tz
from ..models import *
from ..forms import *

# Create your views here.

def index(request):
    ''' home page '''
    context = {'title': 'Inicio', 'page':"index"}
    return render(request, 'pueblomagico/index.html', context)

def travel(request):
    ''' It handles the search destination engine '''
    travels = Viaje.objects.all()
    towns_state = Pueblo.objects.all().select_related()
    target = (ts for ts in towns_state for tr in travels if ts.id==tr.pueblo_id)
    search = { (t.id,t.nombre,t.estado.nombre) for t in target }
    search =[{
        "id":s[0],
        "info": "{} | {}".format(s[1],s[2])
    } for s in search ]
    context = {
        'title': 'Viaja',
        'page':"travel",
        'search':search,
        'breadcrumbs': [{
            "title": "Destino",
            "url": "/travel/"
        }]
    }

    if request.method == 'POST':
        form = TravelForm(request.POST)
        if form.is_valid():
            #travel contains search info from the context used in the search bar engine
            travel = form.cleaned_data["travel"]
            id_travel = [t["id"] for t in search if t["info"]==travel][0]
            id_travel = str(int(id_travel))
            return HttpResponseRedirect(id_travel+"/")
        
        return render(request, 'pueblomagico/travel.html', context)
    else:
        return render(request, 'pueblomagico/travel.html', context)


def choose(request,puebloid:'i32'):
    ''' It shows reservation choices for the chosen town, id is the town's  '''
    if request.method == "GET":
        travel = Viaje.objects.all().filter(pueblo=puebloid).select_related()
        town = Pueblo.objects.select_related().get(pk=puebloid)
        context = {
            'title': 'Comprar', 
            'page':"travel",
            "town": town,
            "travel":travel, 
            'breadcrumbs': [{
                "title": "Destino",
                "url": "/travel/"
                },{
                "title": "Viaje",
                "url": f"/travel/{puebloid}"
            }]
        }
        return render(request, 'pueblomagico/choose.html', context)
    else:
        return redirect(puebloid)

def reserve(request,puebloid,viajeid):
    ''' handles the requests to make a reservation for the travel chosen '''
    travel = Viaje.objects.select_related().get(pk=viajeid)
    town = Pueblo.objects.select_related().get(pk=puebloid)
    context = {
        'title': 'Comprar', 
        'page':"travel",
        "town": town,
        "travel":travel, 
        'breadcrumbs': [{
            "title": "Destino",
            "url": "/travel/"
            },{
            "title": "Viaje",
            "url": f"/travel/{puebloid}"
            },{
            "title": "Reservación",
            "url": f"/travel/{puebloid}/{viajeid}"
        }]
    }
    if request.method == "POST":
        form = ReserveForm(request.POST)
        if form.is_valid():
            camion = Viaje.objects.select_related("camion").get(pk=viajeid).camion
            name = form.cleaned_data["name"]
            lastname = form.cleaned_data["lastname"]
            seats = form.cleaned_data["seats"]
            email = form.cleaned_data["email"]
            cost = form.cleaned_data["cost"]
            ticket = Boleto.objects.create(
                nombre = name,
                apellido = lastname,
                viaje = Viaje.objects.get(pk=viajeid),
                costo_total = cost,
                fecha = tz.now()
            )
            # to make sure there is enough seats to be bought by the client
            if camion.asientos >= seats:
                ticket.asientos = seats
                camion.asientos -= seats
                camion.save()
                ticket.cantidad = seats
                ticket.save()
                return redirect(f"/ticket/{ticket.id}")
        return redirect(f"/travel/{puebloid}/{viajeid}")
    else:
        return render(request, 'pueblomagico/reserve.html', context)

def ticket(request,ticketid):
    if request.method == "GET":
        ticket = Boleto.objects.select_related().get(pk=ticketid)
        context = {
            'title': 'Boleto', 
            'page':"travel",
            "ticket": ticket
        }
        return render(request, 'pueblomagico/ticket.html', context)
    else:
        return redirect("/travel/")
