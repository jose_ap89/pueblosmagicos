from django import forms

class TravelForm(forms.Form):
    travel = forms.CharField(label='Ingrese el destino',max_length=100)

class CreateUserForm(forms.Form):
    user = forms.CharField(max_length=100)
    name = forms.CharField(max_length=100)
    lastname = forms.CharField(max_length=100)
    email = forms.CharField(max_length=100)
    password1 = forms.CharField(max_length=100)
    password2 = forms.CharField(max_length=100)
    currPage = forms.CharField(max_length=100)

class LoginUserForm(forms.Form):
    inputUser = forms.CharField(max_length=100)
    inputPassword = forms.CharField(max_length=100)
    currPage = forms.CharField(max_length=100)

class CreateCommentForm(forms.Form):
    user = forms.IntegerField()
    texto = forms.CharField()
    pueblo = forms.IntegerField()
    currPage = forms.CharField(max_length=100)

#Can be used for put and delete methods to edit or delete comments
#_method can handle PUT , DELETE and GET
class EditCommentForm(forms.Form):
    texto = forms.CharField()
    currPage = forms.CharField(max_length=100)
    _method = forms.CharField(max_length=6)

# handler will have the type of form to handle: reserve or payment 
class ReserveForm(forms.Form):
    name = forms.CharField(max_length=100)
    lastname = forms.CharField()
    seats = forms.IntegerField()
    email = forms.CharField(max_length=100)
    cost = forms.DecimalField()
    numerocuenta = forms.CharField(max_length=100)
    vencimiento = forms.DateField()
    cv = forms.IntegerField()
