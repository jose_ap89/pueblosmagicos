/* validation stateLogin object */
let stateLogin = {
    user : false,
    pwd : false,
    isValid: function() {
        return this.user && this.pwd;
    }
};

let userlogin  = $( "#userLogin" );
let pswdlogin = $( "#passwordLogin" );
let btnlogin = $("#btnLogin");

$("#linkUserLogin").click(function() {
    btnlogin.removeClass( ["btn-secondary","btn-primary"] ).addClass( "btn-secondary" );
    $("#formLoginUser input").each( function(){
        let elem = $(this);
        if (elem.prop("type")!=="hidden"){
            elem.val("");
        }
    });
    $(".msgerrorLogin").remove();
});

/* validation for the user input */
$(userlogin).blur(function() {
    userlogin.val(userlogin.val().trim());
    let text = userlogin.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerrorLogin msgerrorUserLogin mt-2' style='font-size:14px;' >Nombre de Usuario no válido</p>";
        $(".msgerrorUserLogin").remove();
        $( "#errorUserLogin" ).append( err_txt );
        stateLogin.user = false;
    } else if(/[^a-z0-9A-Z\._\-@]+/.test(text)){
        err_txt = "<p class='alert alert-danger msgerrorLogin msgerrorUserLogin mt-2' style='font-size:14px;' >Nombre de Usuario debe tener solo carácteres alfanuméricos sin espacios ni acentos. Símbolos permitidos: @._-</p>";
        $(".msgerrorUserLogin").remove();
        $( "#errorUserLogin" ).append( err_txt );
        stateLogin.user = false;
    } else if(text.length < 3 || text.length > 50) {
        err_txt = "<p class='alert alert-danger msgerrorLogin msgerrorUserLogin mt-2' style='font-size:14px;' >Nombre de Usuario tiene que ser >=3 y <=50 carácteres</p>"
        $(".msgerrorUserLogin").remove();
        $( "#errorUserLogin" ).append(  err_txt );
        stateLogin.user = false;
    } else {
        $(".msgerrorUserLogin").remove();
        stateLogin.user = true;
    }
});

/* validation for pasword inputs */
$(pswdlogin).blur(function() {
    let err_txt = "";
    if( !pswdlogin.val()) {
        err_txt = "<p class='alert alert-danger msgerrorLogin msgerrorPasswordLogin mt-2' style='font-size:14px;' >Debes introducir tu contraseña</p>";
        $(".msgerrorPasswordLogin").remove();
        $( "#errorPasswordLogin" ).append( err_txt );
        pswdlogin.css({"border": "2px solid #ff6347", "border-radius": "4px"});
        stateLogin.pwd = false;
    } else {
        pswdlogin.css({"border": "", "border-radius": ""});
        $(".msgerrorPasswordLogin").remove();
        stateLogin.pwd = true;
    }
});

$("#userLogin, #passwordLogin").blur(function() {
    console.log(stateLogin);
    btnlogin.removeClass( ["btn-secondary","btn-primary"] ).addClass( "btn-secondary" );
    if (stateLogin.isValid()) {
        btnlogin.removeClass( "btn-secondary" ).addClass( "btn-primary" );
    } 
});

$("#formLoginUser").submit( ()=> {
    if(stateLogin.isValid()===false) {
        event.preventDefault();
    }
});
