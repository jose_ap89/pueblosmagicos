/* validation stateReserve object */
let stateReserve = {
    name : false,
    lastname : false,
    email : false,
    numerocuenta : false,
    vencimiento : false,
    cv : false,
    isValid: function() {
        return this.name && this.lastname && this.email &&  this.numerocuenta && this.vencimiento && this.cv ;
    }
};

let nameVal  = $( "#nameVal" );
let lastnameVal  = $( "#lastNameVal" );
let emailVal = $( "#emailVal" );
let numerocuenta = $( "#numeroCuentaVal" );
let vencimiento = $("#vencimientoVal");
let cv = $("#cvVal");

let btnVal = $("#reserveForm button");

/* validation for the name input */
nameVal.blur(function() {
    nameVal.val(nameVal.val().trim());
    const regex = RegExp('[^A-Za-zÀ-ÖØ-öø-ÿ ]+');
    let text = nameVal.val();
    let err_txt = "";
    if(nameVal.prop("readonly")){
        stateReserve.name = true;
    } else if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorNameVal mt-2' style='font-size:14px;' >Nombre no válido</p>";
        $(".msgerrorNameVal").remove();
        $( "#errorNameVal" ).append( err_txt );
        stateReserve.name = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorNameVal mt-2' style='font-size:14px;' >Nombre no debe tener números ni carácteres especiales</p>";
        $(".msgerrorNameVal").remove();
        $( "#errorNameVal" ).append( err_txt );
        stateReserve.name = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorNameVal mt-2' style='font-size:14px;' >Nombre tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorNameVal").remove();
        $( "#errorNameVal" ).append(  err_txt );
        stateReserve.name = false;
    } else {
        $(".msgerrorNameVal").remove();
        stateReserve.name = true;
    }
});

/* validation for the last name input */
lastnameVal.blur(function() {
    lastnameVal.val(lastnameVal.val().trim());
    const regex = RegExp('[^A-Za-zÀ-ÖØ-öø-ÿ ]+');
    let text = lastnameVal.val();
    let err_txt = "";
    if(lastnameVal.prop("readonly")){
        stateReserve.lastname = true;
    } else if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastNameVal mt-2' style='font-size:14px;' >Apellido no válido</p>";
        $(".msgerrorLastNameVal").remove();
        $( "#errorLastNameVal" ).append( err_txt );
        stateReserve.lastname = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastNameVal mt-2' style='font-size:14px;' >Apellido no debe tener números ni carácteres especiales</p>";
        $(".msgerrorLastNameVal").remove();
        $( "#errorLastNameVal" ).append( err_txt );
        stateReserve.lastname = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastNameVal mt-2' style='font-size:14px;' >Apellido tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorLastNameVal").remove();
        $( "#errorLastNameVal" ).append(  err_txt );
        stateReserve.lastname = false;
    } else {
        stateReserve.lastname = true;
        $(".msgerrorLastNameVal").remove();
    }
});

/* validation for email */
emailVal.blur(function() {
    emailVal.val(emailVal.val().trim());
    let text = emailVal.val();
    let err_txt = "";
    if(emailVal.prop("readonly")){
        stateReserve.email = true;
    } else if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmailVal mt-2' style='font-size:14px;' >Email no válido</p>";
        $(".msgerrorEmailVal").remove();
        $( "#errorEmailVal" ).append( err_txt );
        stateReserve.email = false;
    } else if(/[^a-z0-9A-Z\._\-@]+/.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmailVal mt-2' style='font-size:14px;' >Email debe tener solo carácteres alfanuméricos sin espacios ni acentos. Símbolos permitidos: @._-</p>";
        $(".msgerrorEmailVal").remove();
        $( "#errorEmailVal" ).append( err_txt );
        stateReserve.email = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmailVal mt-2' style='font-size:14px;' >Email tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorEmailVal").remove();
        $( "#errorEmailVal" ).append(  err_txt );
        stateReserve.email = false;
    } else if(!/.+@.+\..+/g.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmailVal mt-2' style='font-size:14px;' >Email no válido</p>"
        $(".msgerrorEmailVal").remove();
        $( "#errorEmailVal" ).append(  err_txt );
        stateReserve.email = false;
    }  else {
        $(".msgerrorEmailVal").remove();
        stateReserve.email = true;
    }
});

vencimiento.blur( function(){
    let txt = vencimiento.val();
    let err_txt = "";
    console.log("date: "+"'"+txt+"'");
    if (txt===""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorVencimientoVal mt-2' style='font-size:14px;' >Fecha no válida</p>";
        $(".msgerrorVencimientoVal").remove();
        $( "#errorVencimientoVal" ).append( err_txt );
        stateReserve.vencimiento = false;
    } else {
        $(".msgerrorVencimientoVal").remove();
        stateReserve.vencimiento = true;
    }
});

numerocuenta.blur(function() {
    numerocuenta.val(numerocuenta.val().trim());
    const regex = RegExp('[^0-9]+');
    let text = numerocuenta.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorNumeroCuentaVal mt-2' style='font-size:14px;' >Número de Cuenta no válido</p>";
        $(".msgerrorNumeroCuentaVal").remove();
        $( "#errorNumeroCuentaVal" ).append( err_txt );
        stateReserve.numerocuenta = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorNumeroCuentaVal mt-2' style='font-size:14px;' >Número de Cuenta solo debe contener numeros</p>";
        $(".msgerrorNumeroCuentaVal").remove();
        $( "#errorNumeroCuentaVal" ).append( err_txt );
        stateReserve.numerocuenta = false;
    } else if(text.length < 9 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorNumeroCuentaVal mt-2' style='font-size:14px;' >Número de Cuenta tiene que ser >=9 y <=30 carácteres</p>"
        $(".msgerrorNumeroCuentaVal").remove();
        $( "#errorNumeroCuentaVal" ).append(  err_txt );
        stateReserve.numerocuenta = false;
    } else {
        stateReserve.numerocuenta = true;
        $(".msgerrorNumeroCuentaVal").remove();
    }
});

cv.blur(function() {
    cv.val(cv.val().trim());
    const regex = RegExp('[^0-9]+');
    let text = cv.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorCvVal mt-2' style='font-size:14px;' >CV no válido</p>";
        $(".msgerrorCvVal").remove();
        $( "#errorCvVal" ).append( err_txt );
        stateReserve.cv = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorCvVal mt-2' style='font-size:14px;' >CV solo debe contener números</p>";
        $(".msgerrorCvVal").remove();
        $( "#errorCvVal" ).append( err_txt );
        stateReserve.cv = false;
    } else if(text.length != 3) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorCvVal mt-2' style='font-size:14px;' >CV tiene que ser ==3 carácteres</p>"
        $(".msgerrorCvVal").remove();
        $( "#errorCvVal" ).append(  err_txt );
        stateReserve.cv = false;
    } else {
        stateReserve.cv = true;
        $(".msgerrorCvVal").remove();
    }
});

$("#nameVal, #lastNameVal,  #numeroCuentaVal, #vencimientoVal, #emailVal, #cvVal ").blur(function() {
    btnVal.removeClass( ["btn-secondary","btn-success"] ).addClass( "btn-secondary" );
    if (stateReserve.isValid()) {
        btnVal.removeClass( "btn-secondary" ).addClass( "btn-success" );
    } 
    console.log(stateReserve);
});

$( document ).ready(function() {
    btnVal.removeClass( ["btn-secondary","btn-success"] ).addClass( "btn-secondary" );
    if( $("#reserveForm input[readonly]").length > 0 ){
        stateReserve.name = stateReserve.lastname = stateReserve.email = true;
    }
});

$("#reserveForm").submit( function() {
    if(stateReserve.isValid()===false) {
        event.preventDefault();
    } else { 
        let total = $("#reserveForm #montoAcumulado").val().replace(/[$,]/g,"");
        $("#reserveForm #montoAcumulado").val(total);
    }
});


