/* validation stateCreate object */
let stateCreate = {
    user : false,
    name : false,
    lastname : false,
    email : false,
    pwd : false,
    isValid: function() {
        return this.user && this.name && this.lastname && this.email  && this.pwd;
    }
};

let user  = $( "#user" );
let name  = $( "#name" );
let lastname  = $( "#lastname" );
let email  = $( "#email" );
let pswd1 = $( "#password1" );
let pswd2 = $( "#password2" );
let btn = $("#btnCreateUser");

$("#linkCreateUser").click(function() {
    btn.removeClass( ["btn-secondary","btn-primary"] ).addClass( "btn-secondary" );
    $("#formCreateUser input").each( function(){
        let elem = $(this);
        if (elem.prop("type")!=="hidden"){
            elem.val("");
        }
    });
    $(".msgerror").remove();
});

/* validation for the username */
$("#user").blur(function() {
    user.val(user.val().trim());
    let text = user.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorUser mt-2' style='font-size:14px;' >Nombre de Usuario no válido</p>";
        $(".msgerrorUser").remove();
        $( "#errorUser" ).append( err_txt );
        stateCreate.user = false;
    } else if(/[^a-z0-9A-Z\._\-@]+/.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorUser mt-2' style='font-size:14px;' >Nombre de Usuario debe tener solo carácteres alfanuméricos sin espacios ni acentos. Símbolos permitidos: @._-</p>";
        $(".msgerrorUser").remove();
        $( "#errorUser" ).append( err_txt );
        stateCreate.user = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorUser mt-2' style='font-size:14px;' >Nombre de Usuario tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorUser").remove();
        $( "#errorUser" ).append(  err_txt );
        stateCreate.user = false;
    } else {
        $(".msgerrorUser").remove();
        stateCreate.user = true;
    }
});

/* validation for email */
$("#email").blur(function() {
    email.val(email.val().trim());
    let text = email.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmail mt-2' style='font-size:14px;' >Email de Usuario no válido</p>";
        $(".msgerrorEmail").remove();
        $( "#errorEmail" ).append( err_txt );
        stateCreate.email = false;
    } else if(/[^a-z0-9A-Z\._\-@]+/.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmail mt-2' style='font-size:14px;' >Email de Usuario debe tener solo carácteres alfanuméricos sin espacios ni acentos. Símbolos permitidos: @._-</p>";
        $(".msgerrorEmail").remove();
        $( "#errorEmail" ).append( err_txt );
        stateCreate.email = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmail mt-2' style='font-size:14px;' >Email de Usuario tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorEmail").remove();
        $( "#errorEmail" ).append(  err_txt );
        stateCreate.email = false;
    } else if(!/.+@.+\..+/g.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorEmail mt-2' style='font-size:14px;' >Email no válido</p>"
        $(".msgerrorEmail").remove();
        $( "#errorEmail" ).append(  err_txt );
        stateCreate.email = false;
    }  else {
        $(".msgerrorEmail").remove();
        stateCreate.email = true;
    }
});
/* validation for the name input */
$("#name").blur(function() {
    name.val(name.val().trim());
    const regex = RegExp('[^A-Za-zÀ-ÖØ-öø-ÿ ]+');
    let text = name.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorName mt-2' style='font-size:14px;' >Nombre no válido</p>";
        $(".msgerrorName").remove();
        $( "#errorName" ).append( err_txt );
        stateCreate.name = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorName mt-2' style='font-size:14px;' >Nombre no debe tener números ni carácteres especiales</p>";
        $(".msgerrorName").remove();
        $( "#errorName" ).append( err_txt );
        stateCreate.name = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorName mt-2' style='font-size:14px;' >Nombre tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorName").remove();
        $( "#errorName" ).append(  err_txt );
        stateCreate.name = false;
    } else {
        $(".msgerrorName").remove();
        stateCreate.name = true;
    }
});

/* validation for the last name input */
$("#lastname").blur(function() {
    lastname.val(lastname.val().trim());
    const regex = RegExp('[^A-Za-zÀ-ÖØ-öø-ÿ ]+');
    let text = lastname.val();
    let err_txt = "";
    if(text==""){
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastname mt-2' style='font-size:14px;' >Apellido no válido</p>";
        $(".msgerrorLastname").remove();
        $( "#errorLastname" ).append( err_txt );
        stateCreate.lastname = false;
    } else if(regex.test(text)){
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastname mt-2' style='font-size:14px;' >Apellido no debe tener números ni carácteres especiales</p>";
        $(".msgerrorLastname").remove();
        $( "#errorLastname" ).append( err_txt );
        stateCreate.lastname = false;
    } else if(text.length < 3 || text.length > 30) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorLastname mt-2' style='font-size:14px;' >Apellido tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorLastname").remove();
        $( "#errorLastname" ).append(  err_txt );
        stateCreate.lastname = false;
    } else {
        stateCreate.lastname = true;
        $(".msgerrorLastname").remove();
    }
});

/* validation for pasword inputs */
$("#password1, #password2").blur(function() {
    let passwords = $(".userPassword");
    let err_txt = "";
    console.log("passwords:");
    console.log(passwords);
    let text1 = $("#password1").val();
    let text2 = $("#password2").val();
    if( pswd1.val() !== pswd2.val()) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorPassword mt-2' style='font-size:14px;' >Contraseñas no coinciden</p>";
        $(".msgerrorPassword").remove();
        $( "#errorPassword" ).append( err_txt );
        passwords.css({"border": "2px solid #ff6347", "border-radius": "4px"});
        stateCreate.pwd = false;
    } else if(text1.length < 3 || text1.length > 30 || text2.length < 3 || text2.length > 30 ) {
        err_txt = "<p class='alert alert-danger msgerror msgerrorPassword mt-2' style='font-size:14px;' >Contraseña tiene que ser >=3 y <=30 carácteres</p>"
        $(".msgerrorPassword").remove();
        $( "#errorPassword" ).append(  err_txt );
        stateCreate.pwd = false;
    } else {
        passwords.css({"border": "", "border-radius": ""});
        $(".msgerrorPassword").remove();
        stateCreate.pwd = true;
    }
});

$("#password1, #password2, #user, #name, #lastname, #email").blur(function() {
    btn.removeClass( ["btn-secondary","btn-primary"] ).addClass( "btn-secondary" );
    if (stateCreate.isValid()) {
        btn.removeClass( "btn-secondary" ).addClass( "btn-primary" );
    } 
    console.log(stateCreate);
});

$("#formCreateUser").submit( ()=> {
    if(stateCreate.isValid()===false) {
        event.preventDefault();
    }
});


