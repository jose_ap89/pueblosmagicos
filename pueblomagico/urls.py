from django.urls import path
from django.conf.urls import include

from . import views

app_name = 'pueblomagico'
urlpatterns = [
    #ej: /
    path('', views.index, name='index'),
    #ej: travel
    path('travel/', views.travel, name='travel'),
    #ej: travel/puebloid
    path('travel/<int:puebloid>/', views.choose, name='choose'),
    #ej: travel/puebloid/viajeid
    path('travel/<int:puebloid>/<int:viajeid>/', views.reserve, name='reserve'),
    #ej: ticket/ticketid
    path('ticket/<int:ticketid>', views.ticket, name='ticket'),
    #ej: post/
    path('post/', views.post, name='post'),
    #ej: post/id
    path('post/<int:id>/', views.town, name='town'),
    path('post/<int:id>/<int:comment>/', views.editComment, name='editComment'),
    path('users/signup/', views.signup, name='signup'),
    path('users/login/', views.login, name='login'),
    path('users/logout/', views.logout, name='logout')
]
