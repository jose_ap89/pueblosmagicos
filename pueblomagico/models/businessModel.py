from django.db import models
from django.utils import timezone as tz

# Create your models here.
class Estado(models.Model):
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return "{}".format(self.nombre)

class Pueblo(models.Model):
    nombre = models.CharField(max_length=200)
    url = models.CharField(max_length=300)
    texto = models.TextField()
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.nombre)

class Camion(models.Model):
    modelo = models.CharField(max_length=200)
    placas = models.CharField(max_length=200)
    asientos = models.IntegerField(default=50)
    def __str__(self):
        return "{}".format(self.modelo)

class Hotel(models.Model):
    nombre = models.CharField(max_length=200)
    direccion = models.CharField(max_length=200)
    pueblo = models.ForeignKey(Pueblo, on_delete=models.CASCADE)
    def __str__(self):
        return "{}".format(self.nombre)

class Viaje(models.Model):
    fecha = models.DateTimeField()
    costo = models.DecimalField(max_digits=8,decimal_places=2)
    descripcion = models.CharField(max_length=300)
    camion = models.ForeignKey(Camion, on_delete=models.CASCADE)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    pueblo = models.ForeignKey(Pueblo, on_delete=models.CASCADE)
    @property
    def localTime(self):
        return tz.localtime(self.fecha)
    def __str__(self):
        return "{} - {} - {}".format(self.camion,self.hotel, self.pueblo)

class Boleto(models.Model):
    fecha = models.DateTimeField()
    costo_total = models.DecimalField(max_digits=8,decimal_places=2)
    nombre = models.CharField(max_length=300)
    apellido = models.CharField(max_length=300)
    viaje = models.ForeignKey(Viaje, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=1)
    @property
    def localTime(self):
        return tz.localtime(self.fecha)
    def __str__(self):
        return "{} - {} - {}".format(self.nombre,self.apellido, self.viaje)
