from django.db import models
from django.contrib.auth.models import User
from .businessModel import Pueblo
from django.utils import timezone as tz

class Comentario(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    texto = models.TextField()
    publicacion = models.DateTimeField()
    modificacion = models.DateTimeField()
    pueblo = models.ForeignKey(Pueblo, on_delete=models.CASCADE)
    @property
    def localtimeLastUpdate(self):
        return tz.localtime(self.modificacion)
    def __str__(self):
        return "id:{},text:{}".format(self.id,self.texto)
