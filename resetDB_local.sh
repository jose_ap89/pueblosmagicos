#!/bin/bash
# You need to have the python virtual environment activated
# it has two options: 
# reset -> drop and create data base with a clean slate
# update -> the same as reset but load all the available fixtures

sudo -i -u postgres psql <<EOF
\l
DROP DATABASE IF EXISTS pueblosmagicos;
CREATE DATABASE pueblosmagicos;
EOF
echo "...REMOVING PYC FILES..."
find -type f -name "*pyc" -o -name "*0001*" | grep migrations | xargs rm
echo "....MAKING MIGRATIONS...."
python manage.py makemigrations pueblomagico
echo "....MIGRATING...."
python manage.py migrate

[[ $# -eq 0 ]] && echo "DataBase reset to empty state" && exit 0

if [[ $1 -eq  update ]]; then
	echo "....LOADING FIXTURES TO DB...."
	fixtures=$(ls pueblomagico/fixtures/*.json | sort )
	for fixture in $fixtures ; do
		python manage.py loaddata $fixture
	done
elif [[ $1 -eq reset ]]; then
	echo DataBase reset finished to empty state
else
	echo No option found. DataBase reset to empty state
fi
