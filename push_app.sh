#!/bin/bash
# example:  push_app deploy_heroku
# deploy_to_heroku give a: y | Y | n | N to say if you want the app 
# to be deployed to heroku

[[ $# -ne 1  ]] && echo "You need to supply ONE argument: deploy_heroku [y/n]" && exit 1

git log --oneline | head -3
echo "Write down the new name: "
echo -n "> "
read res
git add .
git commit -m "$res"
echo ".......PUSHING TO BITBUCKET....."
git push
echo ".......NEW LOG......."
git log --oneline | head -3

case  $1  in
	y | Y )
	    echo ".......PUSHING TO HEROKU........"
	    git push heroku master
	    echo "Push and deployment successfuly executed"
	    ;;
	n | N )
	    echo "No deployment. Push finished."
	    exit 0
	    ;;            
	*)
  	    echo "Invalid option. Push finished without deployment."
	    exit 0
	    ;;	    
esac

